"use strict";

/**
 * This class manages raw API queries to WWSU.
 * @requires $ jQuery
 * @requires $.alpaca Alpaca forms custom WWSU build
 */

// REQUIRES these WWSUmodules (via WWSUreq): noReq, hostReq, djReq, directorReq, adminDirectorReq
class WWSUapi {
	/**
	 * Construct the class.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		this.manager = manager;

		this.dom; // The DOM string of the API form will be stored here when initialized.
	}

	/**
	 * Make API query to WWSU
	 *
	 * @param {string} path sails.js REST URL path
	 * @param {string} req this.requests key indicating which WWSUreq to use.
	 * @param {object} data Data to pass to API
	 * @param {function} cb Callback executed. Parameter is returned data from WWSU.
	 */
	query(path, req, data, cb) {
		this.manager
			.get(req)
			.request(path, { data }, { dom: this.dom }, (body, resp) => {
				if (resp.statusCode < 400) {
					if (typeof cb === "function") {
						cb(body);
					}
				} else {
					if (typeof cb === "function") {
						cb("Error");
					}
				}
			});
	}

	/**
	 * Initialize Alpaca form for API query.
	 *
	 * @param {*} dom DOM query string where to generate the form.
	 */
	initApiForm(dom) {
		this.dom = dom;
		$(dom).alpaca({
			schema: {
				type: "object",
				properties: {
					path: {
						type: "string",
						title: "REST-style route / API path",
						required: true,
					},
					req: {
						type: "string",
						title: "Authorization to use",
						enum: [
							"noReq",
							"hostReq",
							"djReq",
							"directorReq",
							"adminDirectorReq",
						],
						required: true,
					},
					data: {
						type: "string",
						title: "JSON data to send to API",
					},
					response: {
						type: "string",
						title: "Response from server",
					},
				},
			},
			options: {
				fields: {
					path: {
						helpers: [
							`MUST use the format "VERB /api/path/:param/:optionalParam?"`,
							`For relative paths, begin path with a /. Otherwise, include the protocol forst (eg. https://).`
						]
					},
					data: {
						type: "json",
					},
					response: {
						type: "textarea",
					},
				},
				form: {
					buttons: {
						submit: {
							title: "Submit Query",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									form.focus();
									return;
								}
								let value = form.getValue();
								this.query(value.path, value.req, value.data, (response) => {
									form.setValue({
										path: value.path,
										req: value.req,
										data: value.data,
										response: JSON.stringify(response),
									});
								});
							},
						},
					},
				},
			},
		});
	}
}

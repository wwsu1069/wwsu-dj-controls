"use strict";

// This class manages the EAS

// REQUIRES these WWSUmodules: noReq (WWSUreq), directorReq (WWSUreq) (only if sending new alerts or cancelling existing ones), WWSUMeta, WWSUutil, WWSUanimations
class WWSUeas extends WWSUdb {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super("WWSUeas"); // Create the db

		this.manager = manager;

		this.endpoints = {
			edit: "PUT /api/eas/:ID",
			get: "GET /api/eas",
			remove: "DELETE /api/eas/:ID",
			test: "POST /api/eas/test",
			send: "POST /api/eas",
		};

		this.data = {
			get: {},
			test: {},
			send: {},
		};

		this.table;

		this.displayed = [];

		this.assignSocketEvent("eas", this.manager.socket);

		this.on("change", "WWSUeas", (db) => {
			this.emitNewAlerts();
			this.updateTable();
		});

    this.on("replace", "WWSUeas", () => {
      // WWSUmodules loading DOM check
      this.initialized = true;
      this.manager.checkInitialized();
    });

		this.easModal = new WWSUmodal(`Active Emergency Alerts`, null, ``, true, {
			overlayClose: true,
			zindex: 1100,
			timeout: 180000,
			timeoutProgressbar: true,
		});

		this.newEASModal = new WWSUmodal(`New EAS Alert`, null, ``, true, {
			overlayClose: false,
			zindex: 1200,
		});
	}

	/**
	 * Get the timezone we should use.
	 */
	get timezone() {
		return this.manager.has("WWSUMeta")
			? this.manager.get("WWSUMeta").timezone
			: moment.tz.guess();
	}

	// Start the connection. Call this in socket connect event.
	init() {
    this.initialized = false;
    this.manager.checkInitialized();

		this.replaceData(
			this.manager.get("noReq"),
			this.endpoints.get,
			this.data.get
		);
	}

	/**
	 * Send an alert out through the internal Node.js EAS (but NOT the on-air EAS)
	 *
	 * @param {string} dom DOM query string of the element to block while processing
	 * @param {object} data The data to pass to the API
	 * @param {?function} cb Callback called after API request is made
	 */
	send(dom, data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.send,
			{
				data: data,
			},
			{ dom },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Alert Sent!",
						autohide: true,
						delay: 10000,
						body: `Alert was sent!`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Send a test alert through the internal EAS (but NOT the on-air EAS)
	 *
	 * @param {string} dom DOM query string of the element to block while processing
	 * @param {?function} cb Callback called after API request is made
	 */
	test(dom, cb) {
		this.manager
			.get("directorReq")
			.request(this.endpoints.test, { data: {} }, { dom }, (body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Test Alert Sent!",
						autohide: true,
						delay: 10000,
						body: `Test alert was sent!`,
					});
					if (typeof cb === "function") cb(true);
				}
			});
	}

	/**
	 * Edit and re-send an alert through the EAS
	 *
	 * @param {string} dom DOM query string of the element to block while processing
	 * @param {object} data The data to pass to the API
	 * @param {?function} cb Callback called after API request is made
	 */
	edit(dom, data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.edit,
			{
				data: data,
			},
			{ dom },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Alert Edited!",
						autohide: true,
						delay: 10000,
						body: `Alert was edited and re-sent!`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Remove an EAS alert
	 *
	 * @param {string} dom DOM query string of the element to block while processing
	 * @param {object} data The data to pass to the API
	 * @param {?function} cb Callback called after API request is made
	 */
	remove(dom, data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.remove,
			{
				data: data,
			},
			{ dom },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Alert Removed!",
						autohide: true,
						delay: 10000,
						body: `Alert was removed!`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Emit events for new alerts
	 */
	emitNewAlerts() {
		this.find().forEach((record) => {
			if (this.displayed.indexOf(record.ID) === -1) {
				this.displayed.push(record.ID);
				this.emitEvent("newAlert", [record]);
			}
		});
	}

	/**
	 * Initialize table for managing EAS alerts
	 *
	 * @param {string} table DOM query string of the div where to place the table
	 */
	initTable(table) {
		this.manager.get("WWSUanimations").add("eas-init-table", () => {
			// Init html
			$(table).html(
				`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				}.</p><p><button type="button" class="btn btn-block btn-success btn-eas-new">New EAS Alert</button><button type="button" class="btn btn-block btn-warning btn-eas-test">Send Test</button></p><table id="section-eas-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			this.manager.get("WWSUutil").waitForElement(`#section-eas-table`, () => {
				// Generate table
				this.table = $(`#section-eas-table`).DataTable({
					paging: true,
					data: [],
					columns: [
						{ title: "Source" },
						{ title: "Alert" },
						{ title: "Counties" },
						{ title: "Starts" },
						{ title: "Expires" },
						{ title: "Actions" },
					],
					columnDefs: [{ responsivePriority: 1, targets: 5 }],
					pageLength: 25,
					drawCallback: () => {
						// Action button click events
						$(".btn-eas-edit").unbind("click");
						$(".btn-eas-delete").unbind("click");

						$(".btn-eas-edit").click((e) => {
							let eas = this.find().find(
								(eas) => eas.ID === parseInt($(e.currentTarget).data("id"))
							);
							this.showEASForm(eas);
						});

						$(".btn-eas-delete").click((e) => {
							let eas = this.find().find(
								(eas) => eas.ID === parseInt($(e.currentTarget).data("id"))
							);
							this.manager.get("WWSUutil").confirmDialog(
								`Are you sure you want to <strong>permanently</strong> remove the alert "${eas.alert}"?
                            <ul>
                            <li><strong>Do NOT permanently remove an alert unless it is no longer active / in effect (such as if it is cancelled)</strong></li>
                            <li>This removes the EAS alert and will no longer be displayed internally.</li>
                            </ul>`,
								null,
								() => {
									this.remove(undefined, { ID: eas.ID });
								}
							);
						});
					},
				});

				// Add click event for new EAS button
				$(".btn-eas-new").unbind("click");
				$(".btn-eas-new").click(() => {
					this.showEASForm();
				});

				$(".btn-eas-test").unbind("click");
				$(".btn-eas-test").click(() => {
					this.manager.get("WWSUutil").confirmDialog(
						`<p>Please confirm you want to send out an internal EAS test. When sent, please check the following to ensure it worked:</p>
            <ul>
              <li>Active DJ Controls instances</li>
              <li>Display Signs (both public and internal)</li>
              <li>WWSU website</li>
			  <li>The Guardian Media Group website</li>
			  <li>On the air (Text to Speech)</li>
            </ul>
            <p>The test will be active for 3 minutes.</p>`,
						null,
						() => {
							this.test();
						}
					);
				});

				// Update with information
				this.updateTable();
			});
		});
	}

	/**
	 * Update the DJ management table if it exists
	 */
	updateTable() {
		this.manager.get("WWSUanimations").add("eas-update-table", () => {
			if (this.table) {
				this.table.clear();
				this.find().forEach((eas) => {
					this.table.row.add([
						eas.source,
						eas.alert,
						eas.counties,
						moment
							.tz(
								eas.starts,
								this.manager.has("WWSUMeta")
									? this.manager.get("WWSUMeta").meta.timezone
									: moment.tz.guess()
							)
							.format("lll"),
						moment
							.tz(
								eas.expires,
								this.manager.has("WWSUMeta")
									? this.manager.get("WWSUMeta").meta.timezone
									: moment.tz.guess()
							)
							.format("lll"),
						["The National Weather Service", "Wright State Alert"].indexOf(
							eas.source
						) === -1
							? `<div class="btn-group"><button class="btn btn-sm btn-warning btn-eas-edit" data-id="${eas.ID}" title="Edit certain elements of this alert and re-send it."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-eas-delete" data-id="${eas.ID}" title="Delete / cancel this EAS alert."><i class="fas fa-trash"></i></button></div>`
							: `<div class="btn-group"><button class="btn btn-sm btn-warning" title="Cannot edit an automatic alert." disabled><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger" title="Cannot delete / cancel an automatic alert." disabled><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		});
	}

	/**
	 * Show a form for adding / editing EAS Alerts
	 *
	 * @param {?object} data If editing an alert, the original data.
	 */
	showEASForm(data) {
		this.newEASModal.iziModal("open");
		this.newEASModal.body = ``;

		// Correct timezones in data
		if (data) {
			data.starts = moment
				.tz(
					data.starts,
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				)
				.toISOString(true);
			data.expires = moment
				.tz(
					data.expires,
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				)
				.toISOString(true);
		}

		$(this.newEASModal.body).alpaca({
			schema: {
				title: data ? "Edit/Resend EAS Alert" : "New EAS Alert",
				type: "object",
				properties: {
					ID: {
						type: "number",
					},
					source: {
						type: "string",
						required: true,
						default: "WWSU 106.9 FM",
						readonly: typeof data !== "undefined",
						title: "Source / Origin of Alert",
						maxLength: 255,
					},
					counties: {
						type: "string",
						required: true,
						title: "Counties / Areas Affected",
						maxLength: 255,
					},
					alert: {
						type: "string",
						required: true,
						readonly: typeof data !== "undefined",
						title: "Name of Alert",
						maxLength: 255,
					},
					severity: {
						type: "string",
						required: true,
						readonly: typeof data !== "undefined",
						enum: ["Extreme", "Severe", "Moderate", "Minor"],
						title: "Severity",
					},
					color: {
						type: "string",
						required: true,
						readonly: typeof data !== "undefined",
						title: "Alert Representative Color",
					},
					information: {
						type: "string",
						required: true,
						title: "Alert Information",
						maxLength: 1024,
					},
					starts: {
						format: "datetime",
						title: "Start Date/Time",
					},
					expires: {
						format: "datetime",
						title: "Expiration Date/Time",
					},
					needsAired: {
						type: "boolean",
						title: "Broadcast On the Air?",
						default: true,
					},
				},
			},
			options: {
				fields: {
					ID: {
						type: "hidden",
					},
					source: {
						helpers: [
							"The governing body who issued this alert, such as WWSU 106.9 FM or Wright State University.",
							"<strong>CANNOT BE EDITED once set!</strong>",
						],
						validator: function (callback) {
							let value = this.getValue();
							if (
								value &&
								value.toLowerCase() === "the national weather service"
							) {
								callback({
									status: false,
									message: `You may not send alerts on behalf of the National Weather Service; these are automatic.`,
								});
								return;
							}
							if (value && value.toLowerCase() === "wright state alert") {
								callback({
									status: false,
									message: `You may not send alerts on behalf of the Wright State Alert system; these are automatic. Use Wright State University instead.`,
								});
								return;
							}
							callback({
								status: true,
							});
						},
					},
					counties: {
						helper:
							"Comma-separated list of counties or areas this alert affects.",
					},
					alert: {
						helpers: [
							"The official name of the alert being issued.",
							"<strong>CANNOT BE EDITED once set!</strong>",
						],
					},
					severity: {
						helpers: [
							"The severity of this alert.",
							"Minor = no risk of life or property. Moderate = Minor risk of life and property. Severe = moderate risk of life and property. Extreme = critical risk of life and property.",
							"<strong>CANNOT BE EDITED once set!</strong>",
						],
					},
					color: {
						type: "color",
						helpers: [
							"A color representing this alert; used as a background.",
							"<strong>CANNOT BE EDITED once set!</strong>",
						],
					},
					information: {
						type: "textarea",
						helper:
							"Information pertaining to this alert, such as what it is, what is happening, what and whom it affects, and what people should do.",
					},
					starts: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						picker: {
							inline: true,
							sideBySide: true,
						},
						helper: `Use the station timezone of ${this.timezone}.`,
					},
					expires: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						picker: {
							inline: true,
							sideBySide: true,
						},
						helper: `Use the station timezone of ${this.timezone}. Defaults to now if left blank.`,
						validator: function (callback) {
							let value = this.getValue();
							if (!value) {
								callback({
									status: true,
									message:
										"If you leave this blank, the alert will remain in effect until manually deleted. Do NOT forget to delete it!",
								});
								return;
							}
							callback({
								status: true,
							});
						},
					},
					needsAired: {
						rightLabel: "Yes",
						helper:
							"Should this alert be broadcast over the WWSU airwaves via Text to Speech? Highly recommended for Severe or Extreme alerts.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: data ? "Edit / Re-send Alert" : "Send Alert",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Add timezone to starts and expires and convert to ISO String
								if (value.starts) {
									value.starts = `${moment
										.utc(value.starts, "YYYY-MM-DD hh:mm A")
										.format("YYYY-MM-DD[T]HH:mm:ss")}${moment
										.tz(value.starts, this.timezone)
										.format("ZZ")}`;
								}
								if (value.expires) {
									value.expires = `${moment
										.utc(value.expires, "YYYY-MM-DD hh:mm A")
										.format("YYYY-MM-DD[T]HH:mm:ss")}${moment
										.tz(value.expires, this.timezone)
										.format("ZZ")}`;
								}

								if (data) {
									this.edit(
										`#modal-${this.newEASModal.id}-body`,
										value,
										(success) => {
											if (success) {
												this.newEASModal.iziModal("close");
											}
										}
									);
								} else {
									this.send(
										`#modal-${this.newEASModal.id}-body`,
										value,
										(success) => {
											if (success) {
												this.newEASModal.iziModal("close");
											}
										}
									);
								}
							},
						},
					},
				},
			},
			data: data ? data : {},
		});
	}
}

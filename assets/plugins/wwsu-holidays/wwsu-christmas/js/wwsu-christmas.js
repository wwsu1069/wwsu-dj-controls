// Class that manages christmas effects.

// REQUIRES WWSUmodules: WWSUMeta, WWSUanimations, WWSUhosts

// REQUIRES PureSnow, Bokeh

class WWSUchristmas {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 * @param {number} options.snowflakeCount Number of snowflakes that will be visible on screen when snow is falling
     * @param {number} options.bulbCount Number of bulbs that will be visible on screen when doing bulb effect
	 * @param {string} options.decorationDOM The DOM query string indicating decorations for this holiday (default: .halloween-decorations)
	 * @param {boolean} options.isStudio Set to true if running on the studio display sign; sound effects only play when isStudio is false and options.sounds are provided.
	 * @param {string} options.flyingSantaSprite Sprite path to the animated flying santa
	 * @param {object} options.sounds Options pertaining to sound effects; each is a Howler.js initiated instance of the sound effect
	 * @param {Howler} options.sounds.sleighBells Howler sound effect for Sleigh Bells / Snow
	 * @param {Howler} options.sounds.santa Howler sound effect for Santa
	 */
	constructor(manager, options = {}) {
		this.manager = manager;
		this.options = options;

		this.decorationDOM = this.options.decorationDOM || ".christmas-decorations";

		this.pureSnow = new PureSnow(this.options.snowflakeCount);
        this.bokeh = new Bokeh(this.options.bulbCount);
	}

	// Call this after all WWSU modules are added
	init() {
		this.manager.get("WWSUMeta").on("metaTick", "WWSUchristmas", (meta) => {
			// Every minute, check halloween decorations / bugs
			if (moment(meta.time).seconds() === 0) {
				this.do();
			}
		});
		this.do();
	}

	// Perform / check Christmas effects
	do() {
		// Effects should only be active in December.
		this.manager.get("WWSUanimations").add("wwsu-christmas-snow", () => {
			this.pureSnow.toggle_snow(false);
            this.bokeh.toggle(false);
			$(".flying-santa").remove();

			if (
				moment(this.manager.get("WWSUMeta").meta.time || undefined).month() ===
				11
			) {
                $(".christmas-decorations").removeClass("d-none");
				// Do an effect about once every 5 minutes
				if (Math.random() < 0.2) {
					switch (Math.floor(Math.random() * 3)) {
						case 0: // Snow (only activate when not hosting a broadcast; this is CPU intensive)
							if (
								!this.manager.has("WWSUhosts") ||
								!this.manager.get("WWSUhosts").isHost
							) {
								this.pureSnow.toggle_snow(true);
								// Play sound effect
								if (
									!this.options.isStudio &&
									this.options.sounds &&
									this.options.sounds.sleighBells
								) {
									this.options.sounds.sleighBells.volume(0.5);
									this.options.sounds.sleighBells.play();
								}
							}
							break;
						case 1: // Santa Claus
							// Play sound effect
							if (
								!this.options.isStudio &&
								this.options.sounds &&
								this.options.sounds.santa
							) {
								this.options.sounds.santa.volume(0.5);
								this.options.sounds.santa.play();
							}
							$("body").append(
								`<img class="flying-santa" src="${this.options.flyingSantaSprite}" alt="Santa Be Watchin" />`
							);
							break;
                            case 2: // Lights (only activate when not hosting a broadcast; this is CPU intensive)
							if (
								!this.manager.has("WWSUhosts") ||
								!this.manager.get("WWSUhosts").isHost
							) {
								this.bokeh.toggle(true);
								// Play sound effect
								if (
									!this.options.isStudio &&
									this.options.sounds &&
									this.options.sounds.lights
								) {
									this.options.sounds.lights.volume(0.5);
									this.options.sounds.lights.play();
								}
							}
							break;
					}
				}
			} else {
                $(".christmas-decorations").addClass("d-none");
			}
		});
	}
}

"use strict";

let directors = [];
let djs = [];
let bookings = [];
let metaTime = moment().toISOString(true);
let serverConnected = false;

// Listeners

window.ipc.on.djsChange((event, args) => {
	djs = args;
});
window.ipc.renderer.lockGetDjs();
window.ipc.on.directorsChange((event, args) => {
	directors = args;
});
window.ipc.renderer.lockGetDirectors();
window.ipc.on.bookingsChange((events, args) => {
	bookings = args;

	let rows = ``;

	bookings.forEach((event) => {
		// Populate event in table
		rows += `<div class="row p-1">
		<div class="col text-fuchsia">${
			event.type.endsWith("-booking") ? `Booking` : `Broadcast`
		}</div>
		<div class="col text-warning">${event.hosts}</div>
		<div class="col text-teal">${moment(event.start).format(
			"MM/DD h:mm A"
		)} - ${moment(event.end).format("h:mm A")}</div>
		</div>`;
	});

	// Populate reservations table
	$("#lockdown-reservations").html(`
		<div class="row p-1">
			<div class="col"><strong>Type</strong></div>
			<div class="col"><strong>People reserved</strong></div>
			<div class="col"><strong>Time</strong></div>
		</div>
		${rows}`);
});
window.ipc.renderer.lockGetBookings();
window.ipc.on.metaTimeChange((event, args) => {
	metaTime = args;
	$(".meta-time").html(moment.parseZone(metaTime).format("llll"));
});
window.ipc.renderer.lockGetMetaTime();
window.ipc.on.error((event, arg) => {
	let obj = arg;

	$(document).Toasts("create", {
		class: "bg-danger",
		title: obj.title,
		body: obj.error,
		autohide: true,
		delay: 20000,
		icon: "fas fa-skull-crossbones fa-lg",
	});
});
window.ipc.on.connectionStatus((event, arg) => {
	serverConnected = arg;
	if (arg) {
		$("#disconnected").addClass("d-none");
	} else {
		$("#disconnected").removeClass("d-none");
	}
});

// Click events

// Login as member
$("#lockdown-dj").unbind("click");
$("#lockdown-dj").click(() => {
	promptLogin("DJ", djs, (username, password) => {
		window.ipc.renderer.lockTryDjLogin([username, password]);
	});
});

// login as director
$("#lockdown-director").unbind("click");
$("#lockdown-director").click(() => {
	promptLogin("Director", directors, (username, password) => {
		window.ipc.renderer.lockTryDirectorLogin([username, password]);
	});
});

// Force close DJ Controls
$(".btn-close-djcontrols").unbind("click");
$(".btn-close-djcontrols").click(() => {
	if (!serverConnected) window.ipc.closeDJControls();
});

window.ipc.lockReady([]);

function promptLogin(authName, fdb, cb) {
	let tempModal = new WWSUmodal(
		`${authName} Authorization Required`,
		`bg-danger`,
		``,
		true,
		{
			overlayClose: false,
			zindex: 10000,
			timeout: false,
			closeOnEscape: true,
			closeButton: true,
			onClosed: () => {
				tempModal.iziModal("close");
				$(`#modal-${tempModal.id}`).remove();
				tempModal = undefined;
			},
		}
	);

	tempModal.body = `<p>To perform this action, you must login with ${authName} credentials. Please choose a user, and then type in your password.</p><div id="modal-${tempModal.id}-form"></div>`;

	tempModal.iziModal("open");

	let util = new WWSUutil();
	util.waitForElement(`#modal-${tempModal.id}-form`, () => {
		$(`#modal-${tempModal.id}-form`).alpaca({
			schema: {
				title: `Confirm Action`,
				type: "object",
				properties: {
					username: {
						type: "string",
						title: `User`,
						required: true,
						enum: fdb.map((user) => user.name),
					},
					password: {
						title: "Password",
						format: "password",
						required: true,
					},
				},
			},
			options: {
				form: {
					buttons: {
						submit: {
							title: `Authorize`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									form.focus();
									return;
								}
								let value = form.getValue();
								tempModal.iziModal("close");
								cb(value.username, value.password);
							},
						},
					},
				},
			},
		});
	});
}

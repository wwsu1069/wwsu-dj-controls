// WWSU holidays; halloween decorations and bugs
// REQUIRES WWSU modules: WWSUMeta, WWSUanimations, WWSUhosts (if we want to prevent bats and TV static when DJ Controls is hosting the current broadcast)
// REQUIRES libraries: SpiderController (https://github.com/Auz/Bug/blob/master/bug.js)
// REQUIRES a canvas with the id of tv overlayed on top of everything else
// Do not forget to load the CSS file!
class WWSUhalloween {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 * @param {string} options.decorationDOM The DOM query string indicating decorations for this holiday (default: .halloween-decorations)
	 * @param {string} options.spiderSprite The path to the spider sprite
	 * @param {string} options.batSprite The path to the sprite for the bats
	 * @param {boolean} options.isStudio Set to true if running on the studio display sign; sound effects only play when isStudio is false and options.sounds are provided.
	 * @param {object} options.sounds Options pertaining to sound effects; each is a Howler.js initiated instance of the sound effect
	 * @param {Howler} options.sounds.bats Bats sound effect
	 * @param {Howler} options.sounds.glitch Glitch sound effect
	 * @param {Howler} options.sounds.spider Spider sound effect
	 * @param {Howler} options.sounds.static Static sound effect
	 */
	constructor(manager, options = {}) {
		this.manager = manager;
		this.options = options;

		this.decorationDOM = this.options.decorationDOM || ".halloween-decorations";

		this.bugTimer;
		this.bugs;

		this.batTimer;
		this.bats;

		this.tvTimer;
		this.tvStaticTimer;
		this.tvProcessing = false;
		this.tvCanvas = null;
		this.tvContext = null;
		this.tvTime = 0;
	}

	// Call this after all WWSU modules are added
	init() {
		this.manager.get("WWSUMeta").on("metaTick", "WWSUhalloween", (meta) => {
			// Every minute, check halloween decorations / bugs
			if (moment(meta.time).seconds() === 0) {
				this.do();
			}
		});
		this.tvCanvas = document.getElementById("tv");
		this.tvContext = this.tvCanvas.getContext("2d");

		this.do();
	}

	/**
	 * Check whether or not we should activate Halloween mode, and activate / de-activate accordingly.
	 */
	do() {
		// Halloween should only be active between October 17 and October 31.
		if (
			moment(this.manager.get("WWSUMeta").meta.time || undefined).month() !==
				9 ||
			moment(this.manager.get("WWSUMeta").meta.time || undefined).date() < 17
		) {
			$(this.decorationDOM).addClass("d-none");
			$(".navbar").removeClass(["halloween-sepia", "halloween-inverted"]);
			$(".main-sidebar").removeClass(["halloween-sepia", "halloween-inverted"]);
			$(".content-wrapper").removeClass([
				"halloween-sepia",
				"halloween-inverted",
			]);

			// Stop bugs
			if (this.bugTimer) {
				this.bugs.end();
				this.bugs = undefined;
				clearTimeout(this.bugTimer);
				this.bugTimer = undefined;
			}

			// Stop bats
			if (this.batTimer) {
				this.bats.stop();
				$(".halloween-bat").remove();
				this.bats = undefined;
				this.batTimer = undefined;
			}

			// Stop static
			if (this.tvTimer) {
				clearTimeout(this.tvTimer);
				$("#tv").fadeTo(Math.round(Math.random() * 2750) + 250, 0, () => {
					clearInterval(this.tvStaticTimer);
					this.tvStaticTimer = undefined;
					$("#tv").addClass("d-none");
					this.tvTimer = undefined;
				});
			}
			return;
		} else {
			console.log(
				"Halloween: ACTIVATED (it is between October 17 and October 31)."
			);
			$(this.decorationDOM).removeClass("d-none");
			if (Math.random() >= 0.05) {
				// Every 1/20 calls, do a glitch jumpscare
				$(".navbar").addClass("halloween-sepia");
				$(".main-sidebar").addClass("halloween-sepia");
				$(".content-wrapper").addClass("halloween-sepia");
			} else if (
				!this.manager.has("WWSUhosts") ||
				!this.manager.get("WWSUhosts").isHost
			) {
				// Shake and invert effect

				// Play glitch sound effect
				if (
					!this.options.isStudio &&
					this.options.sounds &&
					this.options.sounds.glitch
				) {
					this.options.sounds.glitch.volume(0.5);
					this.options.sounds.glitch.play();
				}

				// Invert colors
				$(".navbar").removeClass("halloween-sepia");
				$(".main-sidebar").removeClass("halloween-sepia");
				$(".content-wrapper").removeClass("halloween-sepia");
				$(".navbar").addClass("halloween-inverted");
				$(".main-sidebar").addClass("halloween-inverted");
				$(".content-wrapper").addClass("halloween-inverted");

				// Shake body 20 times over 5 seconds
				$("body").effect("shake", { times: 20 }, 5000, () => {
					// Stop glitch sound effect
					if (
						!this.options.isStudio &&
						this.options.sounds &&
						this.options.sounds.glitch
					) {
						this.options.sounds.glitch.stop();
					}

					// Return colors back to normal halloween
					$(".navbar").removeClass("halloween-inverted");
					$(".main-sidebar").removeClass("halloween-inverted");
					$(".content-wrapper").removeClass("halloween-inverted");
					$(".navbar").addClass("halloween-sepia");
					$(".main-sidebar").addClass("halloween-sepia");
					$(".content-wrapper").addClass("halloween-sepia");
				});
			}

			// Start effects
			this.doBugs();
			this.doBats();
			this.doTVStatic();
		}
	}

	doBugs() {
		if (!this.bugTimer) {
			// Set a randomized timer to create spiders between 10 and 30 minutes from now. When the spiders are created, make them exist for 1-5 minutes.
			this.bugTimer = setTimeout(() => {
				console.log("Halloween bugs: Activated.");

				// Make repeater for sound effect
				let soundTimer;
				const soundEffect = () => {
					// Play sound effect
					if (
						!this.options.isStudio &&
						this.options.sounds &&
						this.options.sounds.spider
					) {
						this.options.sounds.spider.volume(0.5);
						this.options.sounds.spider.play();
					}
					if (this.bugTimer)
						soundTimer = setTimeout(
							() => soundEffect(),
							Math.round(Math.random() * 50000) + 10000
						);
				};
				soundEffect();

				// Make spiders
				this.bugs = new SpiderController({
					minDelay: 1,
					maxDelay: 2,
					minBugs: 1,
					maxBugs: 3,
					imageSprite: this.options.spiderSprite,
					canDie: false,
					mouseOver: "nothing",
				});
				this.bugTimer = setTimeout(() => {
					console.log("Halloween bugs: De-activated.");
					// Remove spiders and reset the timers
					this.bugs.end();
					this.bugs = undefined;
					this.bugTimer = undefined;
					clearTimeout(soundTimer);
					this.doBugs();
				}, Math.round(Math.random() * (1000 * 60 * 4)) + 1000 * 60);
			}, Math.round(Math.random() * (1000 * 60 * 20)) + 1000 * 60 * 10);
		}
	}

	doBats() {
		if (!this.batTimer) {
			// Add some bats every 10-30 minutes for 1-3 minutes.
			this.batTimer = setTimeout(() => {
				// Bail if this DJ Controls is hosting the current broadcast; bats can affect remote broadcasts as it uses a lot of CPU / GPU.
				if (
					this.manager.has("WWSUhosts") &&
					this.manager.get("WWSUhosts").isHost
				) {
					this.bats = undefined;
					this.batTimer = undefined;
					this.doBats();
					return;
				}

				// Make repeater for sound effect
				let soundTimer;
				const soundEffect = () => {
					// Play sound effect
					if (
						!this.options.isStudio &&
						this.options.sounds &&
						this.options.sounds.bats
					) {
						this.options.sounds.bats.volume(0.5);
						this.options.sounds.bats.play();
					}
					if (this.bugTimer)
						soundTimer = setTimeout(
							() => soundEffect(),
							Math.round(Math.random() * 60000) + 30000
						);
				};
				soundEffect();

				this.bats = $.halloweenBats({
					image: this.options.batSprite, // Path to the image.
					zIndex: 10000, // The z-index you need.
					amount: Math.round(Math.random() * 4) + 1, // Bat amount.
					width: 35, // Image width.
					height: 20, // Animation frame height.
					frames: 4, // Amount of animation frames.
					speed: 20, // Higher value = faster.
					flickering: 15, // Higher value = slower.
					target: "body", // Target element
				});
				this.batTimer = setTimeout(() => {
					// Remove bats and reset timer
					this.bats.stop();
					$(".halloween-bat").remove();
					this.bats = undefined;
					this.batTimer = undefined;
					clearTimeout(soundTimer);
					this.doBats();
				}, Math.round(Math.random() * (1000 * 60 * 2)) + 1000 * 60);
			}, Math.round(Math.random() * (1000 * 60 * 20)) + 1000 * 60 * 10);
		}
	}

	doTVStatic() {
		// Re-generate static pixels
		const makeNoise = () => {
			let imgd = this.tvContext.createImageData(
				this.tvCanvas.width,
				this.tvCanvas.height
			);
			let pix = imgd.data;

			for (let i = 0, n = pix.length; i < n; i += 4) {
				let c = 7 + Math.sin(i / 50000 + this.tvTime / 7); // A sine wave of the form sin(ax + bt)
				pix[i] = pix[i + 1] = pix[i + 2] = 40 * Math.random() * c; // Set a random gray
				pix[i + 3] = 255;
			}

			this.tvContext.putImageData(imgd, 0, 0);
			this.tvTime = (this.tvTime + 1) % this.tvCanvas.height;
		};

		if (!this.tvTimer && this.tvContext) {
			// Initiate timer to determine when to create static; we should do a static routine every 5-15 minutes.
			this.tvTimer = setTimeout(() => {
				// Bail if this DJ Controls is hosting the current broadcast; TV static can affect remote broadcasts as it uses a lot of CPU / GPU.
				if (
					this.manager.has("WWSUhosts") &&
					this.manager.get("WWSUhosts").isHost
				) {
					this.tvTimer = undefined;
					this.doTVStatic();
					return;
				}
				console.log(`Halloween TV Static: Activated`);
				// Start with visible (but 0 opacity) TV
				$("#tv").removeClass("d-none");
				$("#tv").css("opacity", 0);

				// Initialize sound
				if (
					!this.options.isStudio &&
					this.options.sounds &&
					this.options.sounds.static
				) {
					this.options.sounds.static.volume(0);
					this.options.sounds.static.loop(true);
					this.options.sounds.static.play();
				}

				// Activate static rotation every 50 milliseconds
				this.tvStaticTimer = setInterval(() => {
					if (!this.tvProcessing) {
						this.tvprocessing = true;
						this.manager.get("WWSUanimations").add("tv-static", () => {
							makeNoise();
							this.tvProcessing = false;
						});
					}
				}, 50);

				// Activate randomized opacity swings
				const opacityChange = () => {
					let duration = Math.round(Math.random() * 2750) + 250;
					let intensity = Math.random() * Math.random() * Math.random();

					// Adjust volume
					if (
						!this.options.isStudio &&
						this.options.sounds &&
						this.options.sounds.static
					) {
						this.options.sounds.static.fade(
							this.options.sounds.static.volume(),
							intensity * 0.75,
							duration
						);
					}

					$("#tv").fadeTo(duration, intensity, () => {
						if (Math.random() >= 0.1) {
							opacityChange();
						} else {
							console.log(`Halloween TV Static: Deactivating`);
							duration = Math.round(Math.random() * 2750) + 250;

							// Adjust volume
							if (
								!this.options.isStudio &&
								this.options.sounds &&
								this.options.sounds.static
							) {
								this.options.sounds.static.fade(
									this.options.sounds.static.volume(),
									0,
									duration
								);
							}

							$("#tv").fadeTo(duration, 0, () => {
								console.log(`Halloween TV Static: De-activated`);

								// Stop sound
								if (
									!this.options.isStudio &&
									this.options.sounds &&
									this.options.sounds.static
								) {
									this.options.sounds.static.stop();
								}

								clearInterval(this.tvStaticTimer);
								$("#tv").addClass("d-none");
								this.tvTimer = undefined;
								this.tvStaticTimer = undefined;
								this.doTVStatic();
							});
						}
					});
				};
				opacityChange();
			}, Math.round(Math.random() * (1000 * 60 * 10)) + 1000 * 60 * 5);
		}
	}
}

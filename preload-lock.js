"use strict";
// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.

const { ipcRenderer, contextBridge } = require("electron");

contextBridge.exposeInMainWorld("ipc", {
	getDirectors: () => ipcRenderer.sendSync("getDirectors"),
	getDjs: () => ipcRenderer.sendSync("getDjs"),
	getBookings: () => ipcRenderer.sendSync("getBookings"),
	getMetaTime: () => ipcRenderer.sendSync("getMetaTime"),
	closeDJControls: () => ipcRenderer.sendSync("closeDJControls"),
	lockReady: (args) => ipcRenderer.send("lockReady", args),
	on: {
		djsChange: (fn) =>
			ipcRenderer.on("djsChange", (event, ...args) => {
				fn(null, ...args);
			}),

		directorsChange: (fn) =>
			ipcRenderer.on("directorsChange", (event, ...args) => {
				fn(null, ...args);
			}),

		bookingsChange: (fn) =>
			ipcRenderer.on("bookingsChange", (event, ...args) => {
				fn(null, ...args);
			}),

		metaTimeChange: (fn) =>
			ipcRenderer.on("metaTimeChange", (event, ...args) => {
				fn(null, ...args);
			}),

		error: (fn) =>
			ipcRenderer.on("error", (event, ...args) => {
				fn(null, ...args);
			}),

		connectionStatus: (fn) =>
			ipcRenderer.on("connectionStatus", (event, ...args) => {
				fn(null, ...args);
			}),
	},

	renderer: {
		lockGetDjs: (arg) => ipcRenderer.send("renderer", ["lockGetDjs", arg]),
		lockGetDirectors: (arg) => ipcRenderer.send("renderer", ["lockGetDirectors", arg]),
		lockGetBookings: (arg) => ipcRenderer.send("renderer", ["lockGetBookings", arg]),
		lockGetMetaTime: (arg) => ipcRenderer.send("renderer", ["lockGetMetaTime", arg]),
		lockTryDjLogin: (arg) => ipcRenderer.send("renderer", ["lockTryDjLogin", arg]),
		lockTryDirectorLogin: (arg) => ipcRenderer.send("renderer", ["lockTryDirectorLogin", arg]),
	},
});

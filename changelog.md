# Changelog for wwsu-dj-controls

## 9.0.0 - 2022-04-29

## Changed

- [BREAKING] WWSUconfig re-done with the new config system. See the file / jsdocs.
- [BREAKING] All WWSU modules edited with new v9 API endpoints.
- [BREAKING] WWSUrecipients.registerPeer renamed to WWSUrecipients.editRecipientComputer. Takes parameters (data (object), cb (function)). See jsDoc.
- [BREAKING] WWSUstatus.recorder renamed to WWSUstatus.status and now requires name property on the data parameter.
- [BREAKING] WWSUreq now requires WWSUutil for REST URL parsing via. added WWSUutil.restToObject.
- [BREAKING] WWSUlikedtracks now allows liking of manual tracks in addition to ones played in RadioDJ. Event emitters return liked track record instead of track ID.
- [BREAKING] Now using Dropzone instead of Blueimp in Alpaca.
- [BREAKING] WWSUreq.request now takes parameters (url, reqOpts, opts, cb). See jsDoc.
  - By extension, WWSUdb.replaceData's path paremeter renamed to url and requires a REST-style URL.
  - url parameter requires REST-style URL string (eg. "VERB /path/to/endpoint/:param"). Parameters will be filled in with the data object provided in reqOpts.data. The function parsing this is WWSUutil.restToObject.
  - cb parameter function now returns two parameters: (body, res).
  - WWSUreq.request now handles displaying HTTP status code error toasts itself if opts.hideErrorToast is false or not defined.

## Added

- [BREAKING] WWSUreq.request will now add a "X-CSRF-Token" header to requests not of method get, head, or options; it will fetch this via jQuery AJAX at the URL the WWSUmodules socket is connected to, path /csrfToken .
- WWSUutil.restToObject.
- WWSUonerror module for catching uncaught errors and displaying Toasts.
- "bypassRadioDJ" parameter on WWSUcalendar.verify(); if true, verify will not error if playlistID or eventID is not provided for relevant event types (set to true when we are not using RadioDJ).
- Callout warnings at the top of the forms when going live / remote / sports / sports remote when system is not configured to use RadioDJ. Warns show hosts they must manually control the automation system in addition to DJ Controls.
- Additional prompts when trying to close DJ Controls if responsible for delay system, silence detection, or recording. But it will not prompt anymore if the socket is disconnected.

### Fixed

- Was unable to report more than one issue at a time
- Conflict resolution was not reversing overrides when the overriding event was re-scheduled outside overridden event's time; it only worked for cancellations.
- Re-scheduling an event that was overridden by another event in conflict resolution was having its re-schedule tossed out in error; this should not happen and re-schedules should register / be allowed.
- Lots of analytics and chart bugs

### Updated

- AdminLTE to 3.2.0
- Plugins
- NPM packages
- Electronjs to v18

## 8.34.0-alpha - 2022-02-06

### Changed

- [BREAKING] WWSUdb constructor parameters changed: now takes lsName to specify a localStorage to use in TAFFY, followed by an initialized TAFFY database (for node.js use).
- Some WWSU models which could utilize cached loading from localStorage (until the server returns updated info) utilize it now in WWSUdb.

## 8.33.0-alpha - UNKNOWN

### Removed

- a11ychecker; it was a premium plugin not included in TinyMCE...

### Added

- Autosave to tinyMCE

### Changed

- Replaced SimpleMDE with EasyMDE

### Updated

- Plugins
  - jQuery UI could not be updated to the latest 1.13 yet; jQuery fileupload UI does not support it.
  - TODO: Replace jQuery fileupload UI; it is no longer maintained.
- Dark Mode colors to be more visible

### Fixed

- Z Index issue with Bootstrap modals
- Conflict check window cancel button did not dismiss the window.
- New/edit schedule had the old priority info modal button; removed.
- Moved version out of logo area into user box to fix padding issue where Underwritings was not reachable on the sidebar.
- jQuery Block does not work well on Bootstrap Modals; switched to using AdminLTE overlays for modals instead and added new domModal opts property to WWSUreq.request. You should use this instead to block modals. But be aware neither jQuery Block nor AdminLTE overlays work everywhere. So we need both.
- Edit Blog modal opens before blog is loaded (with an overlay) so you know it is processing.
- Bug in conflictCheck where duplicates were still being returned because one of the two records have no ID yet (as it was not created).
- Activating an inactive calendar event runs a verify check now just in case; if an org member is marked inactive, an event may not be valid anymore / not have a host.
- Editing an inactive calendar event should have been allowed, but it wasn't. This is fixed.
- Confirmation dialog did not dismiss when the callback fails; it should dismiss before callback is fired.

## 8.32.1-alpha - 2022-01-06

### Changed

- Split windows and linux build job in GitLab CI/CD
- Specified only installers to upload as job artifacts to GitLab due to request limit too large errors

## 8.32.0-alpha - 2022-01-04

### Deprecated

- iziModal support will be removed in WWSUmodals in a future version in support of native Bootstrap modals

### Removed

- iziModal library

### Changed

- [BREAKING] pushstate now uses WWSUnavigation active menu DOM re-processing via processMenu instead of completely replacing the body (which nullified event listeners).
- WWSUnavigation now uses d-none class instead of display: none style.

### Added

- Blogs management
- Shutdown issues
- WWSUnavigation breadcrumb handling
- WWSUnavigation addItem title and URL parameters can now be a string-returning function with the clicked element as a passed parameter.
- WWSUnavigation addItem callback now passes the element clicked as a parameter to activate the menu if applicable.
- Bootstrap modal fallback to WWSUmodals if iziModal is not loaded.
- [UNTESTED] Jitter buffer hint 1s to incoming remote calls; we hope to improve management of network issues.
- Check for packet loss on call quality

## 8.31.1-alpha - 2021-12-10

### Fixed

- Marking events active / inactive did not work because the wrong modal was being provided for jQuery blockUI.

## 8.31.0-alpha - 2021-12-04

### Added

- Video stream viewers count on dash and badge in menu.
- Video stream analytics to org members
- Ratios (listener / video stream to show time) to analytic tables
- Video Stream Viewers line to listeners graph at the top of individual logs
- WWSUcalendar.getEvent for getting info on a specific calendar event including analytics (added analytic buttons to calendar events).

### Changed

- [BREAKING] WWSUdjs.showDJAnalytics moved to WWSUanalytics.showAnalytics with two parameters: (member or show, ID).
- Scheduled show time left moved from dash menu item badge to under the station clock
- Remaining time calculated using calendar system instead of meta; more accurate and less buggy that way.

### Removed

- Emergency alerts widget on dashboard

## 8.30.0-alpha - 2021-12-02

### Added

- OS-native Notifications for non-critical things (new messages, track requests, EAS alerts, etc).
- Additional clarification on what should be done for certain error messages in wwsu-state.
- Support for emailFlags in directors: whether or not a director should be emailed when a track or broadcast is reported by someone.
- Flagged tracks and broadcasts will show up as to-do items in DJ Controls.
- Version number display on the top of sidebar menu.
- Christmas effects.
- Notification when internal EAS is about to send the current host to break to broadcast an alert.
- New fields in EAS form for source and whether or not to broadcast the alert on the air.
- Delay restrictions: If delay system status is critical, remote broadcasts will not be allowed, and in-studio broadcasts will get a warning / confirmation dialog. Also, remote broadcasts cannot return from break when delay isn't working.

### Changed

- [BREAKING] CalendarDb.getEvents now no longer returns events more than one week old regardless of parameters provided.
- Added options for canceling / re-scheduling an event that was overridden by a higher priority event.
- Made the schedule change reason for system overrides more concise / less wordy (only affects future overrides).
- Halloween color effects moved to CSS classes; easier to manage when using multiple holidays.
- Halloween will only activate between October 17 and October 31 instead of the entire month of October.
- Info on EAS tab to be more in line with the current internal EAS system.

### Fixed

- When endTime is < startTime, event duration was greater than 24 hours when it simply should have gone to endTime the next day.
- [UNTESTED] Editing an edited occurrence results in the duration being saved as 1 instead of null (keep current duration). This is probably because tempCal in CalendarDb.verify is only being filled with the relevant scheduleID and not a merging of all of them.
- When a modal that already exists is re-opened, a second DOM was being created with the same id, preventing the modal from working properly. Do not create another DOM if it already exists.
- DJ Controls notifying on new messages during times it should not. It should only notify if: sent to host directly, or sent to DJ and either this DJ Controls started the current broadcast, or no host started it and this DJ Controls is locked to the OnAir studio.
- Edit schedule bug when trying to edit a schedule with a schedule-specific logo or banner set.

## 8.29.2-alpha - 2021-11-08

### Fixed

- Edit schedule bug when trying to edit a schedule with a schedule-specific logo or banner set.

### Changed

- Color scheme (WIP)

## 8.29.1-alpha - 2021-11-08

### Removed

- [BREAKING] WWSUmodal footer setter and getter; use the body instead.
- [BREAKING] WWSUrecipients.initTable; create the table when the modal is loaded instead.

### Changed

- [BREAKING] WWSUmodal is no longer on the DOM until it is opened, and it is removed when closed. iziModal should be opened before setting body content via jQuery.
- Operations Logs and Lockdown Logs are now two separate menu tabs to allow for more room.
- Now using stable AdminLTE releases instead of master branch (but npm plugins are being updated to the latest version).
- Durations in calendar schedules split into start time and end time for easier UI.
- One-time schedules are now dates only; they utilize specified start time, now a required field.
- DJ Controls will no longer auto reload (due to the lockdown system) if we reach an unauthorized screen.
- Modified daily cleanup and authorization messages on administration menus to be more clear.
- DJ notes language to member notes.
- Disallow deleting or marking inactive event ID 1; this will serve as a permanent test event.
- Made event type field options on calendar more descriptive.

### Added

- New window icon for menu tabs that open a browser.
- Image uploading support for logos/banners (calendar) and avatars (directors/members). It's a little buggy at the moment but it works.
- Org member permission: "Is a Sports Broadcaster", which members with this set will have a green "Sports Broadcaster" ribbon on the member page of the website.
- Additional announcements locations / types for the new pages on the website.

### Fixed

- Calendar system was still ignoring oneTime schedules that did not fall within the startDate / endDate range; system was hacking an endDate together.

### Updated

- Electron.js to latest 15.x.x
- NPM packages

## 8.29.0-alpha - 2021-11-08

BROKEN

## 8.28.1-alpha - 2021-10-23

### Added

- Small text shadows to add pop

### Changed

- Made adjustments to tomorrow.io weather; tomorrow.io is conservative about mentioning chances of precip in weatherCode, so an override was added such that if any precipIntensity and precipType is defined, weatherCode will be overwritten with the precip conditions.
- Current weather on weather page

## 8.28.0-alpha - 2021-10-23

### Changed

- Many date/time fields no longer allow specifying a timezone and will force using station timezone to ease DST confusion. The one exception is adding a log (during a broadcast) which will ask to use your local timezone; sometimes a remote show might be done outside of the station timezone.
- Time fields now use 12-hour AM/PM format instead of 24-hour format for simplicity.
- startDate and endDate do not ask for a time anymore; time was not necessary for those fields.
- timezone detection now recommended use in WWSUMeta.timezone get function. To override default timezone (either station timezone or moment.tz.guess) specify timezone option when loading WWSUMeta.
- Using jQuery select2 instead of bootstrap multipleselect for multiselect dropdowns; this resolves the issue of the dropdown being cut off the page if too long and unable to scroll down.

### Added

- Maintenance mode notification in top bar when meta state is not any known state.

### Fixed

- Major bug in calendar conflict checking where bookings were being marked conflicted despite not overlapping shows.
- this.options.sounds undefined error in WWSUhalloween

## 8.27.0-alpha - 2021-10-22

### Added

- WWSUdb preChange, preReplace, preInsert, preUpdate, preRemove events
- preNewMeta event in WWSUMeta. Parameters: (meta object received, old meta)
- Prompt when trying to close DJ Controls when lockDown is specified for that host.
- Clarification on lockdown notifications that the lockdown time displayed is the computer time, not the station time.

### Changed

- Automatic lock down after reservation expiration changed from 3 minutes to 5 minutes.
- Automatic lock down after going off the air is no longer triggered from the end broadcast button; it is programmatically triggered via meta change when the host value is changed and the old value was this host. This addresses potential issues where a show is ended by a means other than clicking the end broadcast button.

### Fixed

- Calendar whoShouldBeIn callback bug
- Animations for remote indication on Audio tab and Call Quality box at the top were using the same ID.
- iziModal bug where previous modals were closing when others opened was fixed again; it snuck back in the assets.
- Calendar priority explanation modal was inaccurate.
- Bookings should be priority 0 by default now, not -1. They should not be allowed to overlap each other.
- DJ Controls was trying to auto lock down, even if it was the host of the current broadcast, if it could no longer detect the current broadcast on the schedule.

## 8.26.0-alpha - 2021-10-11

### Removed

- Cache functionality in calendar; it was too buggy. Will possibly be replaced with something else later.

### Added

- timezone option for WWSUcalendar module to specify a specific timezone to use (such as to override using WWSU timezone with using browser timezone).
- Extra padding for count downs and call quality meter on the top bar.
- recipientsChanged in WWSUrecipientsweb so that WWSUmessagesweb can indicate a visitor's current nickname / label.
- Halloween theme activated during October: Bugs, Bats, and random TV static. More decorations and effects may be added later. Might also add other holidays.

### Changed

- Made PLC in remote audio calls decrease call quality a little less aggressively (0.75% [instead of 1] for every 1 PLC, rounded up with each second).
- incomingSilence more aggressive now with dropping call quality.

### Fixed

- Numerous calendar timezone errors; replaced parseZone with moment-timezone.
- Default active menu item was not executing callback.
- After initial load, message notifications popping up after marking one as unread when none should be notifying.
- DJ Controls was notifying on web messages when not the host of the current broadcast; it should only notify on web messages when it is the host.

## 8.25.0-alpha - 2021-10-04

### Added

- Refresh audio devices as well when remote call is restarted due to poor call quality.
- Callback parameter to WWSUstate.showNextDJModal

### Changed

- Remote call quality meter is now in its own separate block on the top bar instead of under the counter for queue.
- More clear "poor audio call quality" message during remote broadcasts.

### Fixed

- Attempted fix for auto lockdown window not coming up after signing off the air.
- Auto lockdown message should appear if necessary when next DJ prompt is displayed but no was clicked.
- WWSUdb.find() actually allows and properly uses functions now.

## 8.24.2-alpha - 2021-09-27

### Fixed

- Disabled background throttling on renderer process; this was causing timer issues for silence detection, remote broadcasting, and other things when the lockdown screen was active and renderer hidden.
- Do not pop up notifications about remote call disconnections when we are already in break; this is not necessary.

## 8.24.1-alpha - 2021-09-24

### Changed

- Now using a separate browserWindow for the main lockdown screen (just like the black ones). Hopefully this resolves all the issues of lockdown not being fullscreen, on top, or on the wrong monitor.

## 8.23.2-alpha - 2021-09-17

### Removed

- GitLab build only if a tag is present; did not work properly.

### Fixed

- Dumb bug in fullscreen lockdown.

## 8.23.0-alpha - 2021-09-17

### Removed

- [BREAKING] makeCalls setting for hosts; replaced by org member permissions system and new "belongsTo" host setting.

### Changed

- [BREAKING] Moved getListeners and getShowtime functions from WWSUlogs to new WWSUanalytics module.
- DJs now referred to as "members" or "Org Members" because we want to add all members, not just DJs, to the system.
- More clarification on confirmation screen for deleting or marking a member as active/inactive.
- More clarification on confirmation screen for deleting or marking an event as active/inactive.
- Modified info on DJs screen to indicate we should use that not just for on-air DJs but for all org members.
- Lockdown buttons changed to black from fuchsia so member door code button can be fuchsia.
- Calendar co-host selection is now a multi select box instead of 3 separate co-host select boxes.
- Adding a director now requires that there be an org member with the same full name added.
- When an org member's full name is changed, if a director's name matches the original full name, the director's name will be changed to the new value as well. Also works vice versa when changing a director's name.
- Host lock to DJ functionality changed. Now, it is "belongs to..." and you choose an org member.
- For security, when a host's belongsTo is set, the specified org member must also be a director if setting host's admin to true (for the administration menu).
- For security, you can no longer specify a lockdown on a host who has belongs to defined. This is to help prevent locking people out of their computer.
- GitLab CI: Will no longer build unless pushed as a tag.
- Data tables will stay on the current page when re-drawn.

### Added

- Underwriting regulations in the underwritings tab for reference.
- WWSUanalytics module for handling analytics and charts
- Permissions field on org members; can specify who is allowed to do remote broadcasts, live shows, book studios, etc.
- Door Code field in member add/edit allowing to specify their door code for later reference by directors.
- Door code access field in member add/edit to specify which doors their code accesses.
- When an org member is marked inactive or deleted, hosts that belong to the org member will have their authorized setting changed to false automatically.
- When a director is deleted, hosts whose belongsTo is set to the org member of the director will have their admin (administration menu) setting set to false automatically.
- Notes when someting requires admin director authorization.
- Restrictions as to which directors are allowed to send emails through the emails tab of DJ Controls.
- Potential MacOS support?

### Fixed

- Bug where if an occurrence was re-scheduled to a date/time outside of the startDate and endDate range, it would not register (eg. treated as expired). We assume if someone deliberately re-schedules an event occurrence outside its start/end date range, it is an exception to that rule and should therefore still register as an active occurrence.
- Attempted fix where main DJ Controls window was not moving to primary display when activating lockdown.
- Add a Log was not prompting non-hosts of current broadcast.
- Alpaca form bugs.

### Updated

- ElectronJS to 14.0.0
- AdminLTE
- Plugins

## 8.22.1-alpha - 2021-08-17

### Fixed

- Kiosk mode does not "always on top", so that was re-added to lockdown.
- More "event is invalid" errors when marking events as inactive.

## 8.22.0-alpha - 2021-08-16

### Added

- Delay System / serial support under Web Serial API is now working

### Changed

- [BREAKING] Updated DJ analytics to match new data structure from API.

### Fixed

- Unhandled exception errors if delay system not set
- Main window is still on top even when logged in; it should not always be on top once logged in.

## 8.21.5-alpha - 2021-08-05

### Fixed

- Lockdown windows for multiple screens not appearing after re-locking
- lockdown.html was in the wrong location
- Added more checks for window closes where a window might already be destroyed

## 8.21.4-alpha - 2021-08-04

### Fixed

- Lockdown windows for multiple screens not exiting when they should
- Lockdown windows for multiple screens white when they should be black

## 8.21.3-alpha - 2021-08-03

### Added

- Power Save Blocker when DJ Controls is on a lock-down machine. This will hopefully stop the OS login screen from interfering with DJ Controls.

## 8.21.2-alpha - 2021-07-25

### Removed

- DiscordWindow stuff no longer being used

### Changed

- [BREAKING] Now using GitLab instead of GitHub
- Now using GitLab Ci instead of Travis Ci

### Added

- Host lockdown support: production, onair, and director. Via hosts admin menu, you can now have DJ Controls on certain hosts "lock" the machine and require login (only allowing DJs to log in at certain times).

### Updated

- Electron.js
- npm packages

## 8.20.1-alpha - 2021-06-10

### Fixed

- Typo preventing checklist at end of show from appearing

## 8.20.0-alpha - 2021-06-09

### Added

- Cleanup and timeout logs added to Admin to-do panel
- Time out counter for when a break will time out and the system will go to automation.
- End of broadcast checklist reminder

### Changed

- WWSUqueue timer method in hopes of improving performance
- Queue time color to teal so that time out can be orange
- Do not display queue time during breaks; it's confusing
- Weather formatting
- Use new wwsuclocks module
- When queue is 0 but not yet considered on the air, display "WAIT" instead of "00".
- Using custom iziModal that does not close other modals when one is opened; we want modals to stack.

### Fixed

- Bug in event selection when adding a new occurrance to the calendar; events did not match the correct IDs
- Alpaca did not render input type color when it should have.

## 8.19.0-alpha - 2021-06-01

### Changed

- [BREAKING] WWSU messages now uses simpleMDE instead of tinyMCE; better compatibility with Discord.
- [WIP] Messages tab changed:
  - [BREAKING] WWSUrecipients.\_updateTable renamed to WWSUrecipients.updateTable.
  - [BREAKING] WWSUmessages.updateRecipientsTable removed.
  - Recipients table / modal no longer displays group nor unread messages (since all unread messages are now always visible).
  - all relevant messages are always displayed (not dependent on which recipient is selected)
  - Put the message box to the side on large displays to make it easier to use
  - Discord compatibility added
- WWSU Logo
- Discord tab now opens Discord in a new window instead of being an actual tab with a Titan Embed.

### Added

- Keys at the bottom of tables that have action buttons. That way, you'll know what each button does without having to hover over for a tool tip.
- WWSUehhh (error sound) (checks are added to prevent it playing if there is a chance it would go on the air)
- Engineer email in the error messages when reporting a problem fails (because you can't report a problem via report a problem if report a problem is broken)

### Fixed

- Pulsing was not being removed when it should have for operation buttons
- Event is invalid error was still present when marking calendar events as inactive.
- When adding a new occurrence on the calendar, the list of events to choose from also included inactive ones; it should only include active ones.

### Updated

- AdminLTE
- Plugins
- Electronjs
- NPM packages

## 8.18.4-alpha - 2021-05-16

### Added

- Warnings about host IDs that it should be treated as a password.
- Quick weather badge on weather tab
- Pulsing on live/remote/sports buttons when one is currently scheduled to be on the air
- Pulsing for end broadcast button when the current broadcast should be wrapping up

### Changed

- Inventory system requires a click of a button now to initialize. This saves on data use as inventory is not frequently used but contains many records.
- On the error messages, only display the last 8 characters of a host ID instead of the full ID.
- Pulse border size increased for more visibility

### Fixed

- DJ prompt did not actually open/show when ending a show; it only created itself.

## 8.18.3-alpha - 2021-05-14

### Fixed

- No ID property when editing an underwriting.
- Underwriting schedules were saving as {value, text} objects when they should have been saving as the value of value numbers.

## 8.18.2-alpha - 2021-05-13

### Added

- End broadcast will now send to automation_break when a show ends and a live or sports show is immediately on the schedule. DJ Controls will prompt and ask if the next DJ is ready to begin their show.

### Fixed

- Interference notifications in error were popping up for shows interfering with themselves (no such thing)

## 8.18.1-alpha - 2021-05-12

### Added

- Notifications when an on-air broadcast interferes with another scheduled broadcast.

### Fixed

- Clockwheel was showing canceled-changed events (because the code checked in error for canceled-updated instead of canceled-changed) when it should not have been.
- Schedule is not defined in calendar

## 8.18.0-alpha - 2021-05-10

### Changed

- [BREAKING] Disabled serial / delay support for the time being due to numerous issues both with node-serialport and web serial api
- [BREAKING] Priority 0 events now operate differently; they can overlap other priority 0 events of different types but cannot overlap priority 0 events of the same type.
- [BREAKING] "Show" and "Sports" event types will always override onair-booking event type regardless of priority.
- Updated renderer and WWSUclimacell with new climacell API data
- DJ removal now marks DJ as inactive instead of fully removing them (inactive DJs are deleted after 1 year). However, DJs can be permanently removed after marked inactive.
  - Inactive DJs cannot be chosen for calendar/event hosts and other functions.
- Event list now includes inactive events
- Event removal now involves marking the event inactive first instead of immediately removing it
- Remote dump button is now visible for live and sports broadcasts just in case someone is more used to clicking that than pushing the physical button in the studio.
- Reverted AdminLTE color scheme back to its defaults as it is more compatible with dark mode.
- Styling of meta info on the dashboard

### Removed

- Scheduled Hours tally in timesheets; it was buggy and not necessary since we expect every director to have a set number of scheduled hours.

### Added

- Dark mode (DJ Controls also remembers upon next start-up if you had dark mode active)
- Email tab for writing and sending emails to DJs or directors in the system.
- DJ Notes functionality via Admin -> DJs (can add notes, remote credits, or discipline).
- Callout info boxes indicating logs/records are deleted after 2 years.
- Text/badges to icon columns in tables
- Event listener for WWSUrecipients in WWSUmessages; when a recipient changes, updateRecipient() is called in case the recipient that changed was the active / selected recipient (updates the text at the top of the message window).
- System problems box in the announcements tab for DJs to see
- Tomorrow.io 12-hour forecast clock
- The friendly name of the host is displayed in the DJ Controls menu under the logo
- Bootstrap multiselect

### Fixed

- Calendar actions dropdown did not proceed for editing / rescheduling an occurrance.
- WWSUcalendar should have been using WWSUdjs for determining which DJs can be chosen for event hosts, but it was instead using the available authorization users from djReq.
- doConflictCheck would call event.verify on event removals; this is not necessary and resulted in false errors.
- Responsive table bugs; sometimes actions buttons did not have priority
- Deleting a schedule did not properly display necessary info in confirm action window because we were using an undefined variable
- DJ attendance logs did not have a view log button for canceled / absent records; it should because there is a log for marking it excused or unexcused.
- Silence detection notifications popped up when silence detection reports offline; it should only pop up for actual silence detections.

### Updated

- AdminLTE packages
- NPM packages / Electron

## 8.17.2-alpha - 2021-04-29

### Fixed

- Critical bug in the calendar system: cannot set start date later than end date.

## 8.17.1-alpha - 2021-04-26

### Fixed

- Silence inactive was not triggering every minute silence was inactive like it should have been.

## 8.17.0-alpha - 2021-04-26

BROKEN

### Added

- A few quick buttons on the calendar filters: "Broadcasts" which will turn all filters off except live/remote/sports/prerecord/playlist, "Bookings" which turns everything off except on-air and prod bookings, and "clear all" which turns everything off.

### Removed

- Next show button (see changed section for more info).

### Fixed

- Calendar system was not accounting for multiple schedule overrides correctly.
- Calendar system was not properly adding "canceled-changed" occurrences at original times when occurrences are rescheduled (does not apply in DJ Controls calendar page; this is mainly for the online calendar system).
- Calendar system did not properly list events where the original date/time was outside selected range, but re-scheduled time was.
- Shell is not defined in index.js.
- silence/inactive was being called literally every time audio was detected; it should only be called if silence was detected and is no longer detected.

### Changed

- [BREAKING] CalendarDb.whatIsPlaying(): added "isCanceled" parameter as third parameter. When true, function will also returned canceled events normally scheduled to take place now.
- Made human readable schedule text easier to read.
- "Automation" button is now "End Broadcast"
- The "end show" button was removed. Instead, DJs should always click "End Broadcast" to end their show.
  - In a future version, if a live show is on the schedule, system will go into automation_break and a prompt will ask if the next DJ is in the station. If no is clicked, system will go back to automation_on.
- WWSU moved Discord servers; updated this in DJ Controls.

## 8.16.0-alpha - 2021-04-06

### Added

- Serial support / delay system monitoring re-enabled using new Web Serial API.
  - Does not currently work due to an Electron.js bug!
- Added recorder error reporting and status checking to WWSU.

### Fixed

- Discord window did not allow discord.gg navigation
- Force sports selection when a sports is not scheduled (instead of defaulting in error to Men's Baseball).
- Meta updates sometimes did not update on the UI if inactive and later active.
- WWSUreq token error handling was inconsistent; should always check for errToken (not tokenErr).
- Notification for new message was not displaying.
- "autoHide" property of Bootstrap toasts was invalid; it was supposed to be "autohide".

### Updated

- AdminLTE
- Plugins
- NPM packages
- Electron.js

### Changed

- Silence detection inactive will be triggered every minute silence is not detected. That way, WWSU knows silence monitoring is still active.

## 8.15.0-alpha

### Fixed

- Editing anything with a date/time would cause the date/time to load up in UTC but save in ET (causing times to advance 4-5 hours).
- this.meta in wwsu-calendar-class was undefined; it should have been using this.manager.get("WWSUMeta").
- Duration bug where re-scheduling / editing an event displayed "NaN" for duration when it should have been empty.
- Event name validation fails when editing a sports occurrance (you had to type it in again). There should be no validation if the box is empty and we are editing.
- Event conflict window should not allow clicking "continue" button if an error occurred (aka. trying to schedule the same event more than once in the same time frame).
- WWSU confirmDialog did not force a string comparison; passing a number in for confirmation resulted in the inability to confirm an action.

### Changed

- [BREAKING] We've been in alpha for a long time and plan to take a while longer. Decided to make versions easier by modifying this version according to major.minor.patch schema (and will continue going forward, just with an alpha tag and we will not be bumping major until we reach stable and then make a breaking change [API should be considered unstable for now]).
- Styling tweaks on dashboard
- Table updating events etc moved to the modules script instead of the renderer one.

### Updated

- AdminLTE and packages
- Electron.js to version 12
  - Serial Port / Delay system functionality is scheduled to be re-implemented once Electron updates their docs for web serial API

### Added

- Underwritings management
- Songs module
- Re-activated main messages system alongside Discord chat; Discord chat was not popular.
- Re-added setting on live, remote, sports, and sports remote forms allowing to disable the web chat during the show.
- "Message sent" toast message on successful sending of a message
- Bulletins on Discord tab to indicate messages are not notified in DJ Controls and to use the main Discord app where possible.

## 8.0.0-alpha.34 - 2021-01-21

### Fixed

- Recorder sometimes did not start if UI was inactive.

## 8.0.0-alpha.33 - 2021-01-19

### Removed

- [BREAKING] This version does NOT support delay system / serial ports. This feature has been disabled until Electron 12 / Chrome 89 / Web Serial API is ready. Node-serialport has become too unstable for our standards.

### Changed

- We are no longer creating a new MediaRecorder for every new recording; audio recorder class creates a single MediaRecorder that is used for the lifetime of the process.
- Recorder now dumps blobs every 5 minutes into blob array before saving

### Added

- Process restarting until memory leaks are resolved (recorder restarts after every recording; audio and silence restart every hour)
- [UNTESTED] Maximum recording duration of 3 hours. After that, recorder triggers a new recording.

### Fixed

- Notification windows... again...
- [UNTESTED] Remote process is restarting/reloading once when initiating a remote broadcast; it should not do that
- [PARTIAL] Calendar conflict checking bugs (Still does not work when un-cancelling a recurring event that overrode other events; this will require an API change which will be done in a later version).
- Calls to send to break during remote broadcast no longer queued when DJ Controls is disconnected from WWSU (it should not have done this).
- Recorder was going over the max allowed Opus bitrate; fixed to 128kbps
- Potential infinite loop scenario when recorder closes/restarts after a DJ goes to break

### Updated

- Electron 11.2.0

## 8.0.0-alpha.32 - 2021-01-11

### Added

- Throw error if trying to load a WWSU module that was not yet added
- Try re-requesting remote call if remote host is not connected when someone wants to resume broadcast

### Changed

- [UNTESTED] Disconnect audio call during remote broadcast when on extended break; reconnect when resuming.
- "reload" process command for silence and remote will now open the process if it is not opened instead of ignoring.
- Changes in meta hostCalled or hostCalling will now trigger remote process reloading instead of open (which is ignored if it is already opened, which it should not; it should re-load the process).
- Audio call, if dropped to 0 but later goes up to 100, will not be restarted at next break.
- Made poor audio quality notification message more clear.

### Fixed

- Inventory management bug with meta not defined
- Typo in check to see if remote process should be re-opened when closed, specifically when in sportsremote state.
- Typo in calendar-class
- Typo in remote process

### Updated

- NPM packages

## 8.0.0-alpha.31 - 2020-12-26 - BROKEN

### Fixed

- Unnecessary extra clockwheel updates when DJ Controls becomes active from inactivity.
- Remote process not closing when remote call drops when it should

## 8.0.0-alpha.30.1 - 2020-12-19

### Fixed

- Yet more unacceptable idiotic bugs of mine

## 8.0.0-alpha.30 - 2020-12-19 - BROKEN

### Fixed

- Null errors on audio saving

### Changed

- Now using Titan for Discord Chat; old WidgetBot was very unreliable and did not support banning.
- Re-enabled serialport, however it is still not working at this time. Instead, changed Lovinity/wwsu to not critical-error if no devices are selected to monitor for delay system. Will be disabling that until serialport works again.
- WWSUanimations uses requestAnimationFrame instead of intervals/timeouts.
- WWSUqueue utilizes an idle callback like interface instead of making a bunch of setTimeouts.

### Added

- Quick links to WSU Guardian, WSU Newsroom, WSU Raiders, and Horizon League.

## 8.0.0-alpha.29 - 2020-12-17

### Fixed

- More stupid unacceptable bugs on my part: null paths for recording.

## 8.0.0-alpha.28.3 - 2020-12-17

### Changed

- [BREAKING] Temporarily disabled delay system capabilities due to a blocking bug in @serialport/bindings@9.0.4

### Fixed

- Fixes to errors I made that were unacceptable and should have been tested prior to release.

## 8.0.0-alpha.28 - 2020-12-17 - BROKEN

### Fixed

- More memory leak fix attempts
- Several request bugs

### Updated

- Electron to v11
- node-serialport
- AdminLTE to 3.1.0-rc1
- Node packages/plugins

### Added

- More security in preload scripts
- Exporting data on some relevant datatables in CSV, PDF, or print formats

### Changed

- Prerecorded shows save in a "Prerecord" folder instead of "live"
- Keep displaying queue time even if inaccurate; display hourglass beside the time instead of only the hourglass
- Queue time slightly smaller size text

## 8.0.0-alpha.27 - 2020-12-15

### Added

- More security checks in app

### Changed

- Another memory leak fix attempt using invoke/handle for VU meters instead of send/on

## 8.0.0-alpha.26 - 2020-12-11

### Fixed

- Nasty memory info bug

## 8.0.0-alpha.25 - 2020-12-05

### Fixed

- Nasty sports remote bug

## 8.0.0-alpha.24.1 - 2020-12-04

### Changed

- Disabled access-control-allow-origin for now; it was blocking Widgetbot.

## 8.0.0-alpha.24 - 2020-12-04

### Changed

- When call quality drops to 0, instead of sending the broadcast to break, DJ Controls will pop up a call quality warning (but will not do so again for 5 minutes). If user goes to break within 5 minutes, the remote process and audio call is restarted during the break.
- End Time no longer required on calendar schedules if not using recurring rules.

### Fixed

- CallQuality termination was not working; fixed and changed behavior (see changed section).

### Added

- "Updating user interface" message when DJ Controls was hidden and the UI is catching up elements (intentional defer)
- Link to open Discord in another window in the Discord chat page; recommended to use that instead of the widget
- Ability to add occurrences and schedules to the calendar by click-dragging and selecting a time frame.
- Ability to re-schedule occurrences by resizing or dragging them on the calendar.

## 8.0.0-alpha.23 - 2020-12-03

### Changed

- Replaced chat system with a widget to the WWSU Discord server, which can be used to chat with people in Discord (and those listening from the website also have a widget to chat in Discord).

### Added

- Delay system dumping (whoops, that was supposed to be added in 8.0.0-alpha.18 but wasn't)

## 8.0.0-alpha.22.1 - 2020-11-30

### Fixed

- Bug in meta newMeta event introduced from bug fix attempt in 8.0.0-alpha.22

## 8.0.0-alpha.22 - 2020-11-30

### Fixed

- Nasty bug where the recorder was creating new recordings every 20-30 seconds

## 8.0.0-alpha.21 - 2020-11-30

### Fixed

- Host to Call field (remote broadcasts) empty on non-admin hosts
- Recorder does not start new recordings on genre changes
- Broadcast descriptions not truncating to 256 character maximum when loading "start broadcast" window
- Calendar schedule removing does nothing (errors with schedule not being defined)

## 8.0.0-alpha.20 - 2020-11-23

### Changed

- [BREAKING] Added module manager to manage each of WWSU's classes
- [BUGS POSSIBLE] Switched all var variables to let and const variables
- WWSUNavigation addItem and removeItem now returns "this" so they can be chained

### Fixed

- DJ Controls not auto-reconnecting audio call in remote broadcast when restarted or disconnected from internet
- (admin) Hosts management responsive table not prioritizing actions buttons
- [NOT FULLY TESTED] Attempted to fix memory leak via the Audio VU meters by reducing the number of ipc events it sends.

### Added

- (admin) EAS management
- "use strict" on all WWSU js files to prevent global variable memory leaks
- Check when remote broadcasts go live to ensure a call is active; immediately go to break if not so

### Removed

- DOMContentLoaded, and moved renderer javascript to the bottom of the body.

## 8.0.0-alpha.19 - 2020-11-20

### Fixed

- hex2bin

## 8.0.0-alpha.18 - 2020-11-20

### Removed

- [BREAKING] Config recorder.deviceId and silence.deviceId; to be replaced with audio array, each containing recorder and silence booleans whether or not they should be responsible for silence detection or recording.

### Changed

- Audio system split into multiple processes. Now supports multiple devices for recording / broadcasting / etc and volume adjustments.
- Due to no support for Javascript stereo MP3 recording (and MP3 being properietary), recorder will now save in webm Opus format. README files will be created in each folder explaining this.
- Skyway.js now utilizes authorization (credential token) via WWSU API to help prevent unauthorized use of the Skyway.js app.
- [BREAKING] Master director now uses ID 1 because server spits out a bunch of logs when using ID of 0

### Fixed

- Clockwheel tooltips issue where clock div was preventing tooltips on certain areas of the clockwheel doughnut.
- Added manually triggering window.resize when switching between menu pages as layout sometimes overlaps each other. This trigger forces a recalculation of screen size so content does not overlap.

### Added

- Remote broadcasting via Skyway.js
- Audio call quality meter under the queue time in the top-bar (operations buttons) when doing a remote broadcast. Visible on the sending and receiving DJ Controls.
- Audio limiter for remote broadcasts to help prevent peaks / clipping
- Support for timesheet notes
- Schema validation to config
- Indications of the status for each audio process
- Silence detection
- Splash screen (basic)
- Delay system tracking via serial port
- Ban / discipline management, and Mute/Ban functionality in the messages / chat
- npx electron-builder install-app-deps in .travis.yml to rebuild node-serialport upon compilation of DJ Controls installers
- Silence detection triggered notifications if this DJ Controls started the current broadcast
- Add a Log and a box in the Dashboard that becomes visible, allowing a producer to click it to clear meta when talking
- Live and remote sports broadcasting

### Updated

- Packages

## 8.0.0-alpha.17 - 2020-10-16

### Removed

- Buttons in notifications; they do not work with Electron IPC.

### Added

- Notification when the main UI of DJ Controls crashes.
- Informative messages on certain pages.
- Helper messages for check in due date and check in/out quantity fields in the inventory system.
- originalDuration property to events (for now, only when conflict checking).
- Remote office hours support in timesheet system.
- Support for master director (ID = 0): cannot be removed, disabled as admin, nor edited by anyone other than the master director.

### Changed

- Wording in the event conflicts window to be easier to read.

## 8.0.0-alpha.16 - 2020-10-13

### Added

- Delay system accountability logging
- Hosts management

### Changed

- Using CSS box shadow flashing for nav icons instead of flashing color class, which was unreliable.

### Updated

- Packages and plugins

## 8.0.0-alpha.15

### Added

- Inventory management and check in/out system.

## 8.0.0-alpha.14 - 2020-09-24

### Added

- Director management

### Changed

- Confirmation dialogs now use iziModal and Alpaca instead of iziToast as large iziToast messages do not scroll on mobile / small screens.
- Authorization prompts also use iziModal and Alpaca instead of iziToast.
- Meta info background color on the dashboard reflects the type of broadcast / state of WWSU.

### Fixed

- Changelog file had improper header levels for changed/added/removed/etc, which should have been h3 but were instead h2.
- Admin Director authorization was showing all directors in the dropdown selection instead of only admin directors; this was fixed.
- Open iziModals were on top of "connection lost" etc overlays when the overlays should have been on top of / blocking the modals.

## 8.0.0-alpha.13 - 2020-09-20

### Updated

- AdminLTE and plugins to latest versions
- NPM packages

### Changed

- Now using WCAG AAA compliant colors both in AdminLTE and in calendar colors
- Calendar event text will turn black if event background color is light so it can be read

## 8.0.0-alpha.12 - 2020-09-17

### Removed

- TinyMCE autosave plugin no longer used; caused a bug preventing DJ Controls from closing.

### Changed

- Installer packages. Now, MacOS will use .pkg (instead of .dmg) and Linux will use .deb or .rpm. Windows will still use .exe, but the installer now allows to install on entire machine.

### Fixed

- TinyMCE blocking DJ Controls from closing when clicking the close button (caused by TinyMCE autosave plugin; which we disabled).
- Timesheet edit bug which always required Clocked time in when it was supposed to only require that for certain approval statuses.
- Reduced padding on administration To-do menu item notification numbers so they do not run into the To-do text.
- Typo in recipients/add-computer (should be recipients/add-computers) preventing registering the host as online.

## 8.0.0-alpha.11 - 2020-09-15

### Added

- Timesheets tab for viewing and modifying director timesheets

### Changed

- Some of the language in the update available dialog.

## 8.0.0-alpha.10 - 2020-09-08

### Added

- Notifications when a new version of DJ Controls is available (uses WWSU API to check for updates instead of autoUpdater, which does not work).

## 8.0.0-alpha.9 - 2020-09-06

### Removed

- WebAudioRecorder.js as it no longer works; replacing with browser MediaRecorder.

### Added

- MP3 program recorder

## 8.0.0-alpha.8 - 2020-09-06

### Removed

- Auto update checking; still does not work and is taking more time than it is worth to try and get working.

### Updated

- Travis cache

## 8.0.0-alpha.7 - 2020-09-06

THIS IS A FAILED RELEASE. DO NOT USE.

### Added

- Activated update notification functionality (UNTESTED).

### Updated

- electron
- electron-store
- electron-builder
- np

## 8.0.0-alpha.6 - 2020-09-06

This is the first release of WWSU DJ Controls version 8 using Travis. It is still in alpha, and many things do not work yet. However, a working version was necessary for WWSU prior to exiting alpha stage.

"use strict";

// This class manages chat recipients from a host level.
// NOTE: event also supports 'recipientChanged' emitted when a new recipient is selected. Parameter is the selected recipient.

// REQUIRES these WWSUmodules: hostReq (WWSUreq), WWSUMeta, WWSUutil, WWSUanimations
class WWSUrecipients extends WWSUdb {
	/**
	 * The class constructor.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/recipients",
			addComputer: "POST /api/recipients/host",
			edit: "PUT /api/recipients/host",
		};
		this.data = {
			get: {},
			addComputer: {},
		};

		this.assignSocketEvent("recipients", this.manager.socket);

		this.activeRecipient = null;

		this.formModal = new WWSUmodal(`Select recipient`, null, ``, true, {
			zindex: 1100,
			onClosed: () => {
				// When the modal closes, destroy the table.
				if (this.table) {
					this.table.destroy();
					this.table = undefined;
				}
			},
		});

		// Determines if we connected to the API at least once before
		this.connectedBefore = false;

		// If true, ignores whether or not this host is already connected
		this.development = true;

		// Contains info about the current recipient
		this.recipient = {};

		this.table;

    // Update recipients table whenever there is a change in recipients.
		this.on("change", "WWSUrecipients", (db) => {
			this.updateTable();
		});

    this.on("replace", "WWSUrecipients", () => {
      // WWSUmodules loading DOM check
      this.initialized = true;
      this.manager.checkInitialized();
    });
	}

	// Initialize connection. Call this on socket connect event.
	init() {
    this.initialized = false;
    this.manager.checkInitialized();

		this.replaceData(
			this.manager.get("hostReq"),
			this.endpoints.get,
			this.data.get
		);
	}

	// Create / Open the modal to choose a recipient.
	openRecipients() {
		// Init html
		this.formModal.iziModal("open");

		$(this.formModal.body).html(
			`<table id="recipients-table" class="table table-striped display responsive" style="width: 100%;"></table>`
		);

		this.manager.get("WWSUutil").waitForElement(`#recipients-table`, () => {
			// Generate table
			this.table = $(`#recipients-table`).DataTable({
				paging: true,
				data: [],
				columns: [
					{ title: "Icon" },
					{ title: "Status" },
					{ title: "Name" },
					{ title: "Actions" },
				],
				columnDefs: [
					{ responsivePriority: 1, targets: 3 },
					{ responsivePriority: 2, targets: 2 },
				],
				order: [[2, "asc"]],
				pageLength: 50,
				drawCallback: () => {
					// Action button click events
					$(".btn-recipient-choose").unbind("click");

					$(".btn-recipient-choose").click((e) => {
						let recipient = this.find().find(
							(recipient) =>
								recipient.ID === parseInt($(e.currentTarget).data("id"))
						);
						this.activeRecipient = recipient;
						this.emitEvent("recipientChanged", [recipient]);
						this.formModal.iziModal("close");
					});
				},
			});

			this.updateTable();
		});
	}

	/**
	 * Add this host as a computer recipient to the WWSU API (register as online).
	 *
	 * @param {string} host Name of the host being registered
	 * @param {function} cb Callback; response as first parameter, boolean true = success, false = no success (or another host is already connected) for second parameter
	 */
	addRecipientComputer(host, cb) {
		this.manager.get("hostReq").request(
			this.endpoints.addComputer,
			{
				data: { host: host },
			},
			{},
			(body, resp) => {
				if (resp.statusCode < 400) {
					this.recipient = body;
					if (
						this.connectedBefore ||
						(typeof body.alreadyConnected !== "undefined" &&
							!body.alreadyConnected) ||
						this.development
					) {
						this.connectedBefore = true;
						cb(body, true);
					} else {
						cb(body, false);
					}
				} else {
					this.recipient = {};
					cb({}, false);
				}
			}
		);
	}

	/**
	 * Update the recipients selection table if it exists.
	 */
	updateTable() {
		this.manager.get("WWSUanimations").add("recipients-update-table", () => {
			if (this.table) {
				this.table.clear();
				this.db()
					.get()
					.map((recipient) => {
						this.table.row.add([
							jdenticon.toSvg(recipient.host, 32),
							(recipient.host === "website" &&
								this.manager.has("WWSUMeta") &&
								this.manager.get("WWSUMeta").meta.webchat) ||
							recipient.status !== 0
								? `<span class="badge badge-success">ONLINE</span>`
								: `<span class="badge badge-danger">OFFLINE</span>`,
							recipient.label,
							`<div class="btn-group">
                        <button class="btn btn-sm bg-blue btn-recipient-choose" data-id="${recipient.ID}" title="Select this recipient"><i class="fas fa-mouse-pointer"></i></button>
                        </div>`,
						]);
					});
				this.table.draw(false);
			}
		});
	}

	/**
	 * Edit a computer recipient.
	 *
	 * @param {object} data Data to pass to the API.
	 * @param {?function} cb Callback after API call made. Parameter is true on success, false on failure.
	 */
	editRecipientComputer(data, cb) {
		this.manager
			.get("hostReq")
			.request(this.endpoints.edit, { data }, {}, (body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") {
						cb(false);
					}
				} else {
					if (typeof cb === "function") {
						cb(true);
					}
				}
			});
	}
}
